import { Component, OnInit } from '@angular/core';
import { NewsApiService } from '../services/news-api.service';
import {Article} from '../models/article.model';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  public feed: Article[] = [];
  public requestIsOk: boolean = false;
  public initialized: boolean;

  constructor(private techCrunchService: NewsApiService) {
    this.initialized = false;
  }

  ngOnInit() {
    this.getLatestArticles();
  }

  getLatestArticles() {
    this.techCrunchService.getFeed().subscribe(resp => {
      this.initialized = true;
      this.feed = resp['articles'];
      this.requestIsOk = true;
    }, () => {
      this.requestIsOk = false;
    });
    // scroll to top on articles list updating
    window.scrollTo(0, 0);
  }

}
