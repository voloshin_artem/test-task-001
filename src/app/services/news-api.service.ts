import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()

export class NewsApiService {

  // Using https://newsapi.org instead of Yahoo Pipes
  // The documentation placed on: https://newsapi.org/docs

  // Yup, I know it's not secure.
  private API_KEY: string = 'd8ab2e49f80d4e999dab579e6022761e';
  private FEED_URL: string = 'https://newsapi.org/v2/top-headlines';
  // categories not using for now, but they can ne implemented if needed.
  // private FEED_CATEGORY: string = '';
  private FEED_SOURCES: string = 'techcrunch,the-verge';
  private FEED_ARTICLES_PER_PAGE: number = 10;

  constructor(private http: HttpClient) { }

  getFeed() {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Methods', 'POST, GET');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append(
      'Access-Control-Allow-Headers',
      'X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding');
    headers.append('X-Requested-With', 'XMLHttpRequest');
    const url = `${this.FEED_URL}?sources=${this.FEED_SOURCES}&apiKey=${this.API_KEY}&pageSize=${this.FEED_ARTICLES_PER_PAGE}`;
    return this.http.get(url, {headers});
  }

}
