export class Article {
  public author: string;
  public description: string;
  public publishedAt: string;
  public title: string;
  public url: string;
  public urlToImage: string;
  public source?: object;
}
